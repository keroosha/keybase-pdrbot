ARG BASE_IMAGE=registry.gitlab.com/notpushkin/keybase-base
FROM $BASE_IMAGE
RUN install_packages python3 python3-pip
RUN install_packages python3-setuptools
RUN apt-get update
RUN apt-get install -y python3-wheel

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

RUN pip3 install ipython
COPY app.py app.py
COPY app_entrypoint.sh app_entrypoint.sh
CMD entrypoint.sh ./app_entrypoint.sh
