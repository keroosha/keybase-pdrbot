import asyncio
import logging
import os
import itertools
from random import choice
import json

from pykeybasebot.types import chat1
from pykeybasebot import Bot

logging.basicConfig(level=logging.DEBUG)

USERS = os.environ.get("PDRBOT_USERS", "ты").split(",")

MESSAGES_FIRST = [
    "Woop-woop! That's the sound of da pidor-police!",
    "### RUNNING `TYPIDOR.SH`...",
    "Опять в эти ваши игрульки играете? Ну ладно...",
    "Сейчас поколдуем...",
    "Инициирую поиск **пидора дня**...",
    "Кто сегодня счастливчик?",
    "Осторожно! **Пидор дня** активирован!",
    "Зачем вы меня разбудили...",
    "Итак... кто же сегодня **пидор дня**?",
    "Инициирую поиск **пидора дня**...",
]

MESSAGES_WAITING1 = [
    "Шаманим-шаманим...",
    "Хм...",
    "Военный спутник запущен, коды доступа внутри...",
    "Машины выехали",
    "(Ворчит) А могли бы на работе делом заниматься",
    "Выезжаю на место...",
    "Сканирую...",
    "Интересно...",
    "Шаманим-шаманим...",
    "Сонно смотрит на бумаги",
]

MESSAGES_WAITING2 = [
    "Доступ получен. Аннулирование протокола.",
    "Так, что тут у нас?",
    "Высокий приоритет мобильному юниту.",
    "Ого-го...",
    "Я в опасности, системы повреждены!",
    "Так-так, что же тут у нас...",
    "Не может быть!",
    "Ох...",
    "КЕК!",
    "Что с нами стало...",
]

MESSAGES_RESULT = [
    "Пидор дня обыкновенный, 1шт. - @{}",
    "И прекрасный человек дня сегодня... а нет, ошибка, всего-лишь **пидор** - @{}",
    "Кажется, **пидор дня** - @{}",
    "Ага! Поздравляю! Сегодня **ты пидор** - @{}",
    "Анализ завершен. **Ты пидор**, @{}",
    "Ну ты и **пидор**, @{}",
    """
.∧＿∧ 
( ･ω･｡)つ━☆・*。 
⊂  ノ    ・゜+. 
しーＪ   °。+ *´¨) 
         .· ´¸.·*´¨) 
          (¸.·´ (¸.·'* ☆ ВЖУХ И ТЫ ПИДОР, @{}
    """.strip(),
]

NS_PIDORS = "pidors"


async def load_flip(chat, *, conversation_id, flip_conversation_id, msg_id, game_id):
    res = await chat.execute(
        {"method": "loadflip", "params": {"options": {
            "conversation_id": conversation_id,
            "flip_conversation_id": flip_conversation_id,
            "msg_id": msg_id,
            "game_id": game_id
        }}}
    )
    return res


class Handler:
    async def __call__(self, bot, ev):
        if ev.msg.content.type_name == chat1.MessageTypeStrings.TEXT.value:
            body = ev.msg.content.text.body.rstrip()
            if body == "!pdr":
                res = await bot.kvstore.get(
                    namespace=NS_PIDORS,
                    entry_key=ev.msg.channel.name,
                    team=f'{os.environ["KEYBASE_USERNAME"]},{os.environ["KEYBASE_USERNAME"]}'
                )
                users: list = res.entry_value.split(',')
                await bot.chat.send(ev.msg.channel, choice(MESSAGES_FIRST))
                await bot.chat.send(ev.msg.channel, f'/flip {", ".join(users)}')
            elif body == "!pdrpenetrate":
                print(ev.msg, flush=1)
                res = await bot.kvstore.get(
                    namespace=NS_PIDORS,
                    entry_key=ev.msg.channel.name,
                    team=f'{os.environ["KEYBASE_USERNAME"]},{os.environ["KEYBASE_USERNAME"]}'
                )
                print(res.entry_value, flush=1)
                users: list = res.entry_value.split(',')
                if ev.msg.sender.username in users:
                    await bot.chat.send(ev.msg.channel, "Эй, ты уже в игре!")
                else:
                    users.append(ev.msg.sender.username)
                    await bot.kvstore.put(
                        namespace=NS_PIDORS,
                        entry_key=ev.msg.channel.name,
                        entry_value=','.join(users),
                        team=f'{os.environ["KEYBASE_USERNAME"]},{os.environ["KEYBASE_USERNAME"]}'
                    )
                    await bot.chat.send(ev.msg.channel, f'**OK!* Ты теперь участвуешь в игре "**Пидор Дня**"')

        elif (
            ev.msg.content.type_name == chat1.MessageTypeStrings.FLIP.value
            and ev.msg.channel.topic_type == "chat"
            and ev.msg.sender.username == os.environ["KEYBASE_USERNAME"]
            and ev.msg.content.flip.flip_conv_id != ""
        ):
            for i in itertools.count(1):
                await asyncio.sleep(2)

                try:
                    res = await load_flip(
                        bot.chat,
                        conversation_id=ev.msg.conv_id,
                        flip_conversation_id=ev.msg.content.flip.flip_conv_id,
                        msg_id=ev.msg.id,
                        game_id=ev.msg.content.flip.game_id
                    )
                except Exception as e:
                    res = {"status": {"phase": -1}}

                if res["status"]["phase"] == 2:
                    break

                else:
                    if i == 1:
                        await bot.chat.send(
                            ev.msg.channel,
                            f"_{choice(MESSAGES_WAITING1)}_"
                        )

                    elif i == 2:
                        await bot.chat.send(
                            ev.msg.channel,
                            choice(MESSAGES_WAITING2)
                        )

            await bot.chat.send(
                ev.msg.channel,
                choice(MESSAGES_RESULT).format(
                    res["status"]["resultInfo"]["shuffle"][0]
                )
            )


# class Handler:
#     async def __call__(self, bot, ev):
#         if event.msg.content.type_name == chat1.MessageTypeStrings.FLIP.value:
#             print(event.msg, flush=1)


listen_options = {
    "local": False,
    "wallet": False,
    "dev": True,
    "hide-exploding": False,
    "filter_channel": None,
    "filter_channels": None,
}


if __name__ == "__main__":
    bot = Bot(
        handler=Handler()
    )

    asyncio.run(bot.start(listen_options))
